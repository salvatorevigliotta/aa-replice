﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Pin : MonoBehaviour
{
    public float speed = 20f; //MagicNumber

    private Rigidbody2D rb;
    private bool isPinned = false;

	// Use this for initialization
	void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(!isPinned)
            rb.MovePosition(rb.position + Vector2.up * speed * Time.deltaTime);
	}

    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.tag == "Rotator")
        {
            transform.SetParent(col.transform);
            Score.PinCount++;
            isPinned = true;
        }

        if (col.tag == "Pin")
        {
            //TODO Singleton GameManager
            FindObjectOfType<GameManager>().EndGame();
        }
    }

}
