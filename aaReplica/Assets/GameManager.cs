﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public Rotator rotator;
    public Spawner spawner;
    public Animator animator;

    private bool gameHasEnded = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void EndGame()
    {
        if (gameHasEnded)
            return;


        if (rotator)
            rotator.enabled = false;
        if (spawner)
            spawner.enabled = false;

        animator.SetTrigger("EndGame");
        gameHasEnded = true;
        Debug.Log("End Game!!!");
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

}
