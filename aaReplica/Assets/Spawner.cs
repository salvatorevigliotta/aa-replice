﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    public GameObject PinPrefab;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if(Input.GetButtonDown("Fire1"))
        {
            SpawnPin();
        }
	}

    private void SpawnPin()
    {
        Instantiate(PinPrefab, transform.position, transform.rotation);
    }
}
